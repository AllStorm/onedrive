//
//  ViewController.swift
//  oneDrive-123
//
//  Created by Mark on 15/03/2018.
//  Copyright © 2018 Kaspars. All rights reserved.
//

import UIKit
import WebKit
import Foundation

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    @IBOutlet weak var webView: WKWebView!
    var code = ""
    var mansTokens = ""
    var contents = ""
    
    
    @IBOutlet weak var textBox: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let viaHomePage = "https://login.live.com/oauth20_authorize.srf?client_id=70f2b4c3-f7a4-4664-b6b3-4a1c682fa438&scope=files.read&response_type=code&redirect_uri=https://login.microsoftonline.com/common/oauth2/nativeclient"
        let viaURL = URL(string: viaHomePage)
        let openViaPageRequest = URLRequest(url: viaURL!)
        webView.navigationDelegate = self as WKNavigationDelegate;
        webView.load(openViaPageRequest)
    }
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        if(getQueryStringParameter(url: self.webView!.url!.absoluteString, param: "code") != nil){
            code = getQueryStringParameter(url: self.webView!.url!.absoluteString, param: "code")!
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if (code != ""){
            let url = URL(string: "https://login.live.com/oauth20_token.srf")!
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            let postString = "client_id=70f2b4c3-f7a4-4664-b6b3-4a1c682fa438&redirect_uri=https://login.microsoftonline.com/common/oauth2/nativeclient&code=\(code)&grant_type=authorization_code"
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                let mansDic = self.convertToDictionary(text: responseString!)
                self.mansTokens = mansDic!["access_token"] as! String
                //print("Access Tokens: \(self.mansTokens)")
                self.makeGetCall()
            }
            task.resume()
        }
        decisionHandler(.allow)
    }
    
    func makeGetCall() {
        // Set up the URL request
        let todoEndpoint: String = "https://graph.microsoft.com/v1.0/me/drives/a1ba28be0c3715f3/root/children"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        
        urlRequest.setValue("Bearer \(self.mansTokens)", forHTTPHeaderField: "Authorization")
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        print("error trying to convert data to JSON")
                        return
                }
                // now we have the todo
                // let's just print it to prove we can access it
                self.contents = todo.description
                DispatchQueue.main.async(execute: {() -> Void in
                    self.setText();
                })
                // the todo object is a dictionary
                // so we just access the title using the "title" key
                // so check for a title and print it if we have one
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
    }
    
    func setText(){
        if (self.contents != ""){
            self.textBox.text = self.contents
            self.textBox.isHidden = false
        }
        return
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
